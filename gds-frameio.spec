%define name    gds-frameio
%define version 2.19.6
%define release 1.1

%define daswg   /usr
%define prefix  %{daswg}
%if %{?_verbose}%{!?_verbose:0}
# Verbose make output
%define _verbose_make 1
%else
%if 1%{?_verbose}
# Silent make output
%define _verbose_make 0
%else
# DEFAULT make output
%define _verbose_make 1
%endif
%endif

Name: 		%{name}
Summary: 	gds-frameio 2.19.6
Version: 	%{version}
Release: 	%{release}%{?dist}
License: 	GPL
Group: 		LIGO Global Diagnotic Systems
Source: 	https://software.igwn.org/lscsoft/source//%{name}-%{version}.tar.gz
Packager: 	John Zweizig (john.zweizig@ligo.org)
BuildRoot: 	%{_tmppath}/%{name}-%{version}-root
URL: 		        https://wiki.ligo.org/Computing/DASWG/DMT
BuildRequires: 	gcc gcc-c++ glibc
BuildRequires:  automake autoconf libtool m4 make
BuildRequires:  gzip zlib bzip2
BuildRequires:  ldas-tools-framecpp-devel >= 2.5.8
BuildRequires:  ldas-tools-al-devel
BuildRequires:  zlib-devel
BuildRequires:  boost-devel
BuildRequires:  gds-base-devel >= 2.19.8
BuildRequires:  gds-lsmp-devel >= 2.19.8
Prefix:		      %prefix

%description
GDS Frame IO software

%package base
Summary: 	GDS development files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Provides: gds-base-frameio
Provides: gds-utilities-frameio
Obsoletes: gds-base-frameio < 2.19.5
Obsoletes: gds-utilities-frameio < 2.19.5
Requires: gds-base >= 2.19.8
Requires: gds-lsmp >= 2.19.8
Requires: ldas-tools-framecpp >= 2.5.8
Requires: ldas-tools-al
Requires: boost
Requires: gzip zlib bzip2
Requires: zlib

%description base
GDS Frame IO software

%package devel
Summary: 	GDS development files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       ldas-tools-framecpp-devel >= 2.5.8
Requires:       gds-base-devel >= 2.19.8
Requires:       gds-lsmp-devel >= 2.19.8

%description devel
GDS Frame IO software development files.

%prep
%setup -q

%build
PKG_CONFIG_PATH=%{daswg}/%{_lib}/pkgconfig
ROOTSYS=/usr

export PKG_CONFIG_PATH ROOTSYS
./configure  --prefix=%prefix \
             --libdir=%{_libdir} \
	           --includedir=%{prefix}/include/gds
make V=%{_verbose_make}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete

%files base
%defattr(-,root,root)
%{_bindir}/*
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_includedir}
%{_libdir}/pkgconfig/*
%{_libdir}/*.a
%{_libdir}/*.so

%changelog
* Fri Jan 28 2022 Edward Maros <ed.maros@ligo.org> - 2.19.6-1
- Updated for release as described in NEWS.md

* Fri May 07 2021 Edward Maros <ed.maros@ligo.org> - 2.19.5-1
- Updated for release as described in NEWS.md

* Wed Feb 17 2021 Edward Maros <ed.maros@ligo.org> - 2.19.4-1
- Modified Source field of RPM spec file to have a fully qualified URI

* Tue Feb 02 2021 Edward Maros <ed.maros@ligo.org> - 2.19.3-1
- Corrections to build rules

* Fri Nov 20 2020 Edward Maros <ed.maros@ligo.org> - 2.19.2-1
- Split lowlatency as a separate package

* Thu Jun 11 2020 Edward Maros <ed.maros@ligo.org> - 2.19.1-1
- Removed FrameL
